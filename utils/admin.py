from django.contrib import admin
from django.apps import apps
# from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin
# from utils import models
# @admin.register(models.Section)
# class SectionAdmin(admin.ModelAdmin, DynamicArrayMixin):
#     readonly_fields = ["body", "pk"]
models = apps.get_models()

for model in models:
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass
