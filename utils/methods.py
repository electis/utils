import json
from utils import functions

def send_email_client(client, params):
    return client.send_email(params.get('subject'), params.get('message'))

def gen_data(client, params):
    scope = json.dumps({
        'name': 'products',
        'type': 'list',
        'count': 10,
        'items': {
            'type': 'dict',
            'items': {
                'id': 'int',
                'name': 'str',
                'price': 'float'
            }
        }
    })
    scope = params.get('scope', scope)
    try:
        dic = json.loads(scope)
    except:
        result = {'error': 1}
    else:
        data = functions.dic2data(dic)
        result = {'error': 0, 'data': data}
    return result
