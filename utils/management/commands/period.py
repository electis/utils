from django.core.management.base import BaseCommand
from django.db.models import F, Q
from time import sleep
from datetime import timedelta, datetime
import asyncio
from concurrent.futures import ThreadPoolExecutor
from utils import models, functions


class Command(BaseCommand):

    def handle(self, *args, **options):
        method = Method()
        models.Period.objects.all().update(in_progress=False)
        while True:
            command_list = models.Period.objects.filter(~Q(period=None), in_progress=False, next_run__lte=datetime.now())
            for command in command_list:
                method._async(command)
            sleep(10)


class Method:
    # TODO обновлять command из базы перед изменением
    def __init__(self):
        self.pool = ThreadPoolExecutor(max_workers=5)
        self.loop = asyncio.get_event_loop()

    def _async(self, command):
        method = getattr(self, command.method)
        if method:
            command.in_progress = True
            command.save()
            # method(command)
            self.loop.run_in_executor(None, method, command)

    def _close(self, command):
        command.next_run = datetime.now()+timedelta(minutes=command.period)
        command.in_progress = False
        command.save()

    def parse(self, command):
        res = functions.parse_url(command.param_1, command.param_2)
        if res:
            if not command.last_state:
                models.PeriodLog.objects.create(period=command, result=str(res))
                # command.send_emails(command.param_1, str(res))
                self.loop.run_in_executor(self.pool, command.send_emails, command.param_1, str(res))
            command.last_state = True
        else:
            if command.last_state:
                models.PeriodLog.objects.create(period=command, result=str(res))
                # command.send_emails(command.param_1, str(res))
                self.loop.run_in_executor(self.pool, command.send_emails, command.param_1, str(res))
            command.last_state = False
        self._close(command)
