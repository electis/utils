from django.db import models
from django.utils.crypto import get_random_string
from django.utils import timezone
from utils import functions


class Host(models.Model):
    name = models.CharField(max_length=128, verbose_name="Описание", default='', blank=True)
    url = models.CharField(max_length=128, unique=True, verbose_name="Домен")

    class Meta:
        verbose_name = "Домен"
        verbose_name_plural = "Домены"

    def __str__(self):
        return self.url


class Client(models.Model):
    name = models.CharField(max_length=128, verbose_name="Описание", default='', blank=True)
    key = models.CharField(max_length=64, verbose_name="Ключ доступа", default=get_random_string)
    email = models.EmailField(default='')
    param_1 = models.CharField(max_length=128, verbose_name="Дефолтное_1", default='', blank=True)
    param_2 = models.CharField(max_length=128, verbose_name="Дефолтное_2", default='', blank=True)
    param_3 = models.CharField(max_length=128, verbose_name="Дефолтное_3", default='', blank=True)
    host_allow = models.ManyToManyField(Host, verbose_name="Разрешенные домены", blank=True, related_name='host_allow')

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return '{}'.format(self.name)

    def send_email(self, subj=None, text=None):
        if self.email:
            if subj == None:
                subj = self.param_1
            if text == None:
                text = self.param_2
            return functions.send_email(self.email, subj, text)
        else:
            return None


class Period(models.Model):
    name = models.CharField(max_length=128, verbose_name="Описание", default='', blank=True)
    client = models.ManyToManyField(Client)
    period = models.PositiveIntegerField(verbose_name="Частота запуска (мин)", default=None, blank=True, null=True)
    next_run = models.DateTimeField(verbose_name="Следующий запуск", default=timezone.now, blank=True, null=False)
    method = models.CharField(max_length=64, verbose_name="Метод", default='', blank=False)
    param_1 = models.CharField(max_length=255, verbose_name="Параметр_1", default='', blank=True)
    param_2 = models.CharField(max_length=128, verbose_name="Параметр_2", default='', blank=True)
    in_progress = models.BooleanField(default=False)
    last_state = models.BooleanField(default=None, null=True)

    class Meta:
        verbose_name = "Периодическое задание"
        verbose_name_plural = "Периодические задания"

    def __str__(self):
        return '{}'.format(self.name)

    def send_emails(self, subj=None, text=None):
        for client in self.client.all():
            client.send_email(subj, text)


class PeriodLog(models.Model):
    period = models.ForeignKey(Period, on_delete=models.CASCADE, verbose_name='Задание')
    time = models.DateTimeField(verbose_name='Время запуска', default=timezone.now, blank=True, null=True)
    result = models.CharField(max_length=255, verbose_name="Результат", default='No result', blank=True)

    class Meta:
        verbose_name = "Лог"
        verbose_name_plural = "Логи"

    def __str__(self):
        return '{} ({})'.format(self.period, self.time)
