import random
import string
import requests
from django.core.mail import send_mail
from django.conf import settings


def parse_url(url, text):
    try:
        response = requests.get('http://'+url)
    except:
        return False
    if response.status_code == 200:
        if response.text.find(text) != -1:
            return True
        else:
            return None
    else:
        return response.status_code

def send_email(email, subj, text):
    if type(email) == str:
        email = [email]
    try:
        send_mail(subj, text, settings.EMAIL_HOST_USER, email, fail_silently=False)
    except:
        return False
    else:
        return True

def make_item(typ):
    var = {
        'int': random.randint(0, 999),
        'str': ''.join(random.choice(string.ascii_lowercase) for i in range(10)),
        'float': int(random.random()*1000000)/100
    }
    item = var.get(typ, '')
    return item

def dic2data(dic):
    if isinstance(dic, dict):
        data = {}
        if dic.get('type') == 'list':
            lis = []
            for i in range(dic.get('count', 5)):
                lis.append(dic2data(dic.get('items', {}))) #
            data = {dic.get('name', 'data'): lis}
        elif dic.get('type') == 'dict':
            for key, value in dic.get('items').items():
                data[key] = dic2data(value)
        return data
    else:
        return make_item(dic)
