from django.views.generic import View
from django.http import HttpResponse, JsonResponse

from utils import models, methods


class IncomingClient(View):

    def get(self, request):
        print(
            'GET', request.get_host(), request.META.get('HTTP_USER_AGENT'), request.META.get('REMOTE_ADDR'), sep=' ~ ')
        return self.post(request, params=request.GET)

    def post(self, request, params=None):
        print(
            'POST', request.get_host(), request.META.get('HTTP_USER_AGENT'), request.META.get('REMOTE_ADDR'), sep=' ~ ')
        if not params: params = request.POST
        print(params)
        if params.get('key') and params.get('method'):
            client = models.Client.objects.filter(key=params.get('key')).first()
            if client:
                method = params.get('method')
                try:
                    func = getattr(methods, method)
                except AttributeError:
                    return HttpResponse('Method not allowed', status=422)
                else:
                    result = func(client, params)
                    response = JsonResponse({'result': result}, status=200)
                    response["Access-Control-Allow-Origin"] = "*"
                    response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
                    response["Access-Control-Max-Age"] = "1000"
                    response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
                    return response
            return HttpResponse(status=403)
        return HttpResponse('No params', status=422)
